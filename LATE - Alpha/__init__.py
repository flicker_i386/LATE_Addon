# SPDX-License-Identifier: WTFPL
# By Flicker_i386

# ----------------

# Addon Information
bl_info = {
		"name": "LATE - Alpha",
		"author": "Flicker_i386",
		"version": (0, 0, 0),
		"blender": (2, 79, 7),
		"location": "Properties > Render > Alpha",
		"description": "Fix Realtime Viewport Alpha Blend Mode Based On Transparency.",
		"wiki_url": "",
		"tracker_url": "",
		"category": "Material",
}

# ----------------

# Module
import bpy
from bpy.types import (
											AddonPreferences,
											Operator,
											Panel,
											)

# ----------------

# Panel
class LATEAlphaPanel(bpy.types.Panel):
	bl_label = "Alpha"
	bl_space_type = "PROPERTIES"
	bl_region_type = "WINDOW"
	bl_context = 'render'

	def draw(self, context):
		layout = self.layout
		col = layout.column()
		col.operator('late_alpha.update', icon='MATERIAL')

#User Preference
class LATEAlphaPreference(AddonPreferences):
	# this must match the addon name, use '__package__'
	# when defining this in a submodule of a python package.
	bl_idname = __name__

	def draw(self, context):
		layout = self.layout
		col = layout.column()
		col.label(icon='ERROR', text="Make Sure To Enable Window Multisample In Preferences.")

# ----------------

# Update
class LATEAlphaUpdate(bpy.types.Operator):
	bl_idname = "late_alpha.update"
	bl_label = "Set Viewport Alpha"
	bl_description = "Set Material Viewport Alpha Blend Mode Based On Transparency."
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		for material in bpy.data.materials:
			material.game_settings.alpha_blend = 'ALPHA_ANTIALIASING' if material.use_transparency else 'CLIP'

		return {'FINISHED'}

# ----------------

classes = (
	LATEAlphaPanel,
	LATEAlphaPreference,
	LATEAlphaUpdate,
	)

# Register
def register():
	for cls in classes:
		bpy.utils.register_class(cls)

# Unregister
def unregister():
	for cls in classes:
		bpy.utils.unregister_class(cls)

if __name__ == "__main__":
	register()
