# SPDX-License-Identifier: WTFPL
# By Flicker_i386

# ----------------

# Addon Information
bl_info = {
		"name": "LATE - Game Render",
		"author": "Flicker_i386",
		"version": (0, 0, 0),
		"blender": (2, 79, 7),
		"location": "Properties > Render > Game Render",
		"description": "Render Image Sequence With Game Engine Pass.",
		"warning": "The Only Way To Cancel Ongoing Render Is To Kill It With Process Manager.",
		"wiki_url": "",
		"tracker_url": "",
		"category": "Game Engine",
}

# ----------------

# Module
import bpy
from bpy.props import (
											FloatProperty,
											IntProperty,
											StringProperty,
											PointerProperty,
											)
from bpy.types import (
											AddonPreferences,
											Operator,
											Panel,
											PropertyGroup,
											)

# ----------------

# Variable
class LATERenderProperty(PropertyGroup):
	#Delay
	time_start : FloatProperty(
	name = "Render Start Time",
	default = 0.125,
	min = 0.00625,
	max = 16.0,
	description = "Wait Time For Game To Load"
	)

	time_end : FloatProperty(
	name = "Render End Time",
	default = 0.125,
	min = 0.00625,
	max = 16.0,
	description = "Delay For Game To Write Image"
	)

	# Leading Zero
	leading_zero : IntProperty(
	name = "Leading Zero",
	default = 6,
	min = 1,
	max = 16,
	description = "Leading Zero For Default Frame Number"
	)

	# Object Name As All In One Render Control Container
	container_name = "~ LATE - Game Render"

	# Raw Filepath , Set By User
	path : StringProperty(
	name = "Filepath",
	description = "Image Output File Directory",
	default = "//",
	maxlen = 1024,
	subtype = 'FILE_PATH'
	)

	# Export Filepath , Modified To Append Frame Number And Extension
	# "File_A0" + "32" + ".png" = "File_A0_0032.png"
	path_format : StringProperty(
	name = "Filepath Formatted",
	description = "Image Output File Directory , With Frame Number And Extension",
	default = "//",
	maxlen = 1024,
	subtype = 'FILE_PATH'
	)

# ----------------

# Panel
class LATERenderPanel(bpy.types.Panel):
	bl_label = "Game Render"
	bl_space_type = "PROPERTIES"
	bl_region_type = "WINDOW"
	bl_context = 'render'

	def draw(self, context):
		scene = context.scene
		lgr = scene.late_render_property

		layout = self.layout
		layout.use_property_split = True
		layout.use_property_decorate = False
		col = layout.column()

		if bpy.context.mode == 'OBJECT':
			colprop = col.column(align=True)
			colprop.prop(lgr,"time_start",text="Time Start")
			colprop.prop(lgr,"time_end",text="End")

			col.separator()

			col.prop(lgr, "path", text="")
			row = col.row(align=True)
			row.label(lgr.path_format)
			row.operator('late_render.refresh', icon='FILE_REFRESH', text='')

			col.separator()

			colop = col.column(align=True)
			colop.operator('late_render.render', icon='RENDER_STILL')
			colop.operator('late_render.render_sequence', icon='RENDER_ANIMATION')

		else:
			col.label(icon='INFO', text='Available In Object Mode')

#User Preference
class LATERenderPreference(AddonPreferences):
	# this must match the addon name, use '__package__'
	# when defining this in a submodule of a python package.
	bl_idname = __name__

	def draw(self, context):
		scene = context.scene
		lgr = scene.late_render_property
		layout = self.layout

		col = layout.column()

		row = col.row()
		row.label(text="Default Leading Zero")
		row.prop(lgr, "leading_zero", text="")

		col.separator()

		col.label(icon='INFO', text="Use Hash \"#\" For Frame Number, Double Slash \"//\" For Relative Directory.")

# ----------------

# Remove
class LATERenderRemove(bpy.types.Operator):
	bl_idname = "late_render.remove"
	bl_label = "Remove Render Game"
	bl_description = "Remove Render Game Configuration"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		scene = context.scene
		lgr = scene.late_render_property

		# Delete Existing Container
		for objdel in scene.objects:
			if objdel.name.startswith(lgr.container_name) :
				bpy.data.objects.remove(objdel)

		return {'FINISHED'}

# Setup
# Setup And Update Is The Same , Run Each Frame ,
# Hopefully It Does Not Waste A Lot Of Memory By Deleting And Recreating Object
class LATERenderSetup(bpy.types.Operator):
	bl_idname = "late_render.setup"
	bl_label = "Setup Render Game"
	bl_description = "Setup And Update Render Game Configuration"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		scene = context.scene
		lgr = scene.late_render_property
		opsobj = bpy.ops.object
		opslog = bpy.ops.logic

		# Set Resolution
		scene.game_settings.resolution_x = scene.render.resolution_x
		scene.game_settings.resolution_y = scene.render.resolution_y

		# Stash Selection
		stash_selected = context.selected_objects

		bpy.ops.late_render.remove()

		# Add New Container
		opsobj.empty_add(type='SPHERE')
		container											= context.active_object
		container.name								= lgr.container_name
		container.empty_draw_size			= 0
		container.lock_location[0]		= container.lock_location[1] 	= container.lock_location[2] 	= True
		container.lock_rotation[0]		= container.lock_rotation[1] 	= container.lock_rotation[2] 	= True
		container.lock_scale[0]				= container.lock_scale[1] 		= container.lock_scale[2] 		= True

		# Must Be In Visible Layer To Be Executed
		for i in range(len(container.layers)):
			container.layers[i] = True

# ----------------

		# Add Sensor
		opslog.sensor_add(type='DELAY', name='Start Time', object=container.name)
		opslog.sensor_add(type='DELAY', name='End Time', object=container.name)

		sensor_a = container.game.sensors['Start Time']
		sensor_b = container.game.sensors['End Time']

		sensor_a.use_pulse_true_level = sensor_b.use_pulse_true_level = True
		sensor_a.show_expanded = sensor_b.show_expanded = False

		sensor_a.delay = int(lgr.time_start * scene.game_settings.frequency)
		sensor_b.delay = int((lgr.time_start + lgr.time_end) * scene.game_settings.frequency)

		# Add Controller
		opslog.controller_add(type='LOGIC_AND', name='A', object=container.name)
		opslog.controller_add(type='LOGIC_AND', name='B', object=container.name)

		controller_a = container.game.controllers['A']
		controller_b = container.game.controllers['B']

		controller_a.show_expanded = controller_b.show_expanded = False
		controller_a.use_priority = controller_b.use_priority = True

		# Add Actuator
		opslog.actuator_add(type='GAME', name='Snapshot', object=container.name)
		opslog.actuator_add(type='GAME', name='Exit', object=container.name)

		actuator_a = container.game.actuators['Snapshot']
		actuator_b = container.game.actuators['Exit']

		actuator_a.show_expanded = actuator_b.show_expanded = False

		actuator_a.mode = 'SCREENSHOT'
		actuator_b.mode = 'QUIT'

# ----------------

		# Filename
		filepath_break = list(lgr.path.rsplit("/",1))
		filepath = filepath_break[0]
		filename = filepath_break[1]

		# Define Frame Number With Leading Zero
		def frame_number_with_leading_zero(leading_zero) :
			return str(scene.frame_current).zfill(leading_zero + (scene.frame_current < 0))

		# Foolish Way To Modify A Filepath
		# Replace "./" With "//"
		if filepath.startswith("./") : filepath = filepath.replace("./","//")

# ----------------

		# Is It Directory Or A File
		if filename == "" :
			# Is A Directory

			# Set Filename As Pure Number And Extension
			lgr.path_format = filepath + "/" + frame_number_with_leading_zero(lgr.leading_zero) + ".png"

# ----------------

		else :
			# Is A File

			# Foolish Way To Modify A Filename
			# Delete ".png" Extension If Exist ,
			# Then Add It Again In The End After Hash Number Replacement
			if "." in filename :
				filename_extension = filename.rsplit(".",1)[1]
				if filename_extension.lower() == "png" : filename = filename.replace(filename_extension,"")
				del filename_extension

			# If It Does Not Contain Hash Character , Add Some
			if not "#" in filename :
				filename += "_"
				for i in range(lgr.leading_zero) :
					filename += "#"

			# Replace Hash To Number First
			# By Grouping The Repeated Character
			filename_break = []

			# https://stackoverflow.com/questions/22882922/splitting-a-string-with-repeated-characters-into-a-list
			filename_string = filename
			filename_split_temporary = filename_string[0]
			for i in range(1,len(filename_string)):
					if filename_string[i] == filename_string[i-1]:
							filename_split_temporary += filename_string[i]
					else:
							filename_break.append(filename_split_temporary)
							filename_split_temporary = filename_string[i]
					if i == len(filename_string)-1:
							filename_break.append(filename_split_temporary)
			del filename_string
			del filename_split_temporary

			# Then Pick Latest Hash Occurence
			filename_hash_pointer = len(filename_break)

			for i in range( len(filename_break) - 1, -1, -1) :
				if "#" in filename_break[i] :
					filename_hash_pointer = i
					break

			# After That , Replace Hash Character In Pointer
			# To Frame Number With Equal Leading Zero
			filename_hash_length = len(filename_break[filename_hash_pointer])

			filename_break[filename_hash_pointer] = frame_number_with_leading_zero(filename_hash_length)

			# Wrapping Up
			filename = "".join(filename_break)

			lgr.path_format = filepath + "/" + filename + ".png"

# ----------------

		actuator_a.filename = lgr.path_format

# ----------------

		# Link
		sensor_a.link(controller_a)
		sensor_b.link(controller_b)
		actuator_a.link(controller_a)
		actuator_b.link(controller_b)

		# Deselect
		# Operator Only Run When Selected , So Hiding Object Is Last
		container.hide_viewport								= container.hide_select				= container.hide_render				= True
		container.select = False
		opsobj.select_all(action='DESELECT')

		# Restore Selection
		if len(stash_selected) > 0 :
			for obj in stash_selected:
				obj.select = True
				scene.objects.active = obj
				print(obj , 'IS ACTIVE')

		return {'FINISHED'}

# ----------------

# Refresh
class LATERenderRefresh(bpy.types.Operator):
	bl_idname = "late_render.refresh"
	bl_label = "Refresh Render Game"
	bl_description = "Refresh Render Game Data"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		bpy.ops.late_render.setup()
		bpy.ops.late_render.remove()

		return {'FINISHED'}

# View
class LATERenderView(bpy.types.Operator):
	bl_idname = "late_render.view"
	bl_label = "View Render Game"
	bl_description = "Render Viewport In Game Engine Pass"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		scene = context.scene
		lgr = scene.late_render_property

		if scene.render.engine == 'BLENDER_GAME' :
			bpy.ops.view3d.game_start()
		else :
			previous_render_engine = scene.render.engine
			scene.render.engine = 'BLENDER_GAME'
			bpy.ops.view3d.game_start()
			scene.render.engine = previous_render_engine

		return {'FINISHED'}

# Render
# Unable To Use The Word "Execute" In Code
class LATERenderRender(bpy.types.Operator):
	bl_idname = "late_render.render"
	bl_label = "Render Game Image"
	bl_description = "Render Single Image In Game Engine Pass"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		scene = context.scene
		lgr = scene.late_render_property

		bpy.ops.late_render.setup()

		if scene.render.engine == 'BLENDER_GAME' :
			bpy.ops.wm.blenderplayer_start()
		else :
			previous_render_engine = scene.render.engine
			scene.render.engine = 'BLENDER_GAME'
			bpy.ops.wm.blenderplayer_start()
			scene.render.engine = previous_render_engine

		bpy.ops.late_render.remove()
		print("LATE - Game Render : " + lgr.path_format)

		return {'FINISHED'}

# Render In Loop Based From Frame (Preview) Range
class LATERenderRenderSequence(bpy.types.Operator):
	bl_idname = "late_render.render_sequence"
	bl_label = "Render Game Sequence"
	bl_description = "Render Image Sequence In Game Engine Pass"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		scene = context.scene

		frame_return = scene.frame_current

		frame_start = 0
		frame_end = 0

		if not scene.use_preview_range:
			frame_start = scene.frame_start
			frame_end = scene.frame_end
		else:
			frame_start = scene.frame_preview_start
			frame_end = scene.frame_preview_end

		for frame in range(frame_start , frame_end + 1) :
			scene.frame_set(frame)
			bpy.ops.late_render.render()

		scene.frame_set(frame_return)

		return {'FINISHED'}

# ----------------

classes = (
	LATERenderProperty,
	LATERenderPanel,
	LATERenderPreference,
	LATERenderRemove,
	LATERenderSetup,
	LATERenderRefresh,
	LATERenderView,
	LATERenderRender,
	LATERenderRenderSequence,
	)

addon_keymaps = []

# Register
def register():
	for cls in classes:
		bpy.utils.register_class(cls)

	bpy.types.Scene.late_render_property = PointerProperty(type=LATERenderProperty)

	wm = bpy.context.window_manager
	if wm.keyconfigs.addon:
		km = wm.keyconfigs.addon.keymaps.new(name='Object Mode')
		kmi = km.keymap_items.new("late_render.view", 'P', 'PRESS')
		addon_keymaps.append((km, kmi))

# Unregister
def unregister():
	for cls in classes:
		bpy.utils.unregister_class(cls)

	del bpy.types.Scene.late_render_property

	wm = bpy.context.window_manager
	if wm.keyconfigs.addon:
		for km, kmi in addon_keymaps:
			km.keymap_items.remove(kmi)
	addon_keymaps.clear()

if __name__ == "__main__":
	register()
